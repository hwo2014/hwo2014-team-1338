__author__ = 'shindorinn'

import json
import threading
import time
import Queue
from global_settings import TICK_WAITING_TIME

class Communicator(threading.Thread):

    def __init__(self, socket, bot):
        threading.Thread.__init__(self)
        self.socket = socket
        self.bot = bot
        self.queue = Queue.Queue()

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def msg_to_queue(self, msg_type, data):
        self.queue.put(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def ping(self):
        self.msg("ping", {})

    def join_msg(self, name, key):
        return self.msg("join", {"name": name,
                                 "key": key})

    def on_car_positions(self, data, tick_time=None):
        tick_received = time.time()
        self.bot.on_car_positions(data)

        while self.queue.empty():
            time_last_message = (time.time()-tick_received)*1000
            if time_last_message > TICK_WAITING_TIME:
                self.ping()
                return
        msg = self.queue.get()
        self.send(msg)
        print("Got message from bot \"{bot_name}\": {msg}".format(bot_name=self.bot.name, msg=msg))

    def run(self):
        msg_map = {
            'join': self.bot.on_join,
            'gameStart': self.bot.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.bot.on_crash,
            'gameEnd': self.bot.on_game_end,
            'error': self.bot.on_error,
            'dnf': self.bot.on_dsf
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            try:
                msg = json.loads(line)
            except:
                print("Couldn't read response: {line}".format(line=line))
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
            line = socket_file.readline()