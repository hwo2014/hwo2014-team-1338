from bots.basebot import BaseBot
from communication.communicator import Communicator

__author__ = 'shindorinn'

import json


class FastNoobBot(BaseBot):

    def __init__(self, socket, name, key):
        self.communicator = Communicator(socket, self)
        self.name = name
        self.key = key
        self.socket = socket

    def msg(self, msg_type, data):
        self.communicator.msg_to_queue(msg_type, data)

    def send(self, msg):
        self.communicator.send(msg)

    def join(self):
        return self.communicator.join_msg(self.name, self.key)

    def throttle(self, throttle):
        self.communicator.msg_to_queue("throttle", throttle)

    def ping(self):
        self.communicator.ping()

    def join_race(self):
        self.send(json.dumps({
            "msgType": "joinRace", "data": {
            "botId": {
                "name": self.name,
                "key": self.key
            },
              "trackName": "keimola",
              "password": "RacePopCorn",
              "carCount": 2
            }
        }))

    def run(self):
        self.logic_loop()

    def on_join(self, data):
        print("Joined")

    def on_game_start(self, data):
        print("Race started")

    def on_car_positions(self, data):
        pass

    def on_crash(self, data):
        print("Someone crashed")

    def on_game_end(self, data):
        print("Race ended")

    def on_error(self, data):
        print("Error: {0}".format(data))

    def on_dsf(self, data):
        print("Error: {0}".format(data))

    def logic_loop(self):
        self.communicator.start()
