__author__ = 'shindorinn'

from abc import ABCMeta, abstractmethod
import json


class BaseBot(object):

    __metaclass__ = ABCMeta

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def create_race(self):
        self.send(json.dumps({
            "msgType": "createRace", "data": {
            "botId": {
                "name": self.name,
                "key": self.key
            },
              "trackName": "keimola",
              "password": "RacePopCorn",
              "carCount": 2
            }
        }))

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    @abstractmethod
    def on_join(self, data):
        pass

    @abstractmethod
    def on_game_start(self, data):
        pass

    @abstractmethod
    def on_car_positions(self, data):
        pass

    @abstractmethod
    def on_crash(self, data):
        pass

    @abstractmethod
    def on_game_end(self, data):
        pass

    @abstractmethod
    def on_error(self, data):
        pass

    @abstractmethod
    def logic_loop(self):
        pass

BaseBot.register(tuple)