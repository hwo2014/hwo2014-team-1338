__author__ = 'arjan'

import socket
import sys
import time
import random
from bots.fastnoobbot import FastNoobBot

if len(sys.argv) != 6:
    print("Usage: ./run host port botname1 botname2 botkey")
else:
    host, port, botname1, botname2, key = sys.argv[1:6]
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))

    # Bot1
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot1 = FastNoobBot(s, botname1, key)
    bot1.create_race()
    bot1.run()

    # Bot 2
    s2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s2.connect((host, int(port)))
    bot2 = FastNoobBot(s2, botname2, key)
    bot2.join_race()
    bot2.run()

    time.sleep(1)
    #speed = random.uniform(0.5, 0.655) TODO for more random races
    bot1.throttle(0.655)
    bot2.throttle(0.64)

    # Switching lanes test
    # time.sleep(1)
    # bot1.msg("switchLane", "Right")
    # bot1.throttle(0.3)
